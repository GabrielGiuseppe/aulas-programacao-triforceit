Crie uma classe chamada Aluno com os seguintes Atributos
- nome
- serie
- matricula
- nota1semestre
- nota2semestre

Usando operador ternario [resultado = (condicao) ? "verdade" : "mentira";] 
Resolva o seguinte problema

João esta na 5 série e tirou nota 6,0 no primeiro semestre e nota 5,7 no segundo
Maria esta na 8 série e tirou nota 9,0 no primeiro semestre e nota 4,5 no segundo

Lembrando que: 
Para alunos até a 5 seria a média para passa é de 5,0
Para alunos da 6 a 8 serie a média para passar é de 6,0

Crie um programa que ao buscar um aluno por sua matricula seja impresso o boletim
e se esse aluno esta aprovado ou reprovado como segue o exemplo abaixo


 Nome: [nome]   Matricula: [matricula]    Série: [serie]

 Notas:
 
 1 Semestre : [nota]
 2 Sementre : [nota]

 Declaro por este boletim escolar que [nome] esta [aprovado/reprovado]



