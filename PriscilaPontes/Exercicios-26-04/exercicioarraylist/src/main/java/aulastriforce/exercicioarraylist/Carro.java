package aulastriforce.exercicioarraylist;

public class Carro {

    private String modelo;

    private int ano;

    private String cor;

    private String placa;


    public String getModelo() {
        return this.modelo;
    }

    public int getAno() {
        return this.ano;
    }

    public String getCor() {
        return this.cor;
    }

    public String getPlaca() {
        return this.placa;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }
}