package aulastriforce.exercicioarraylist;

public class Contato {

    private String ddd;

    private int numero;

    private String tipo;

    public String getDdd() {
        return this.ddd;
    }

    public int getNumero() {
        return this.numero;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}