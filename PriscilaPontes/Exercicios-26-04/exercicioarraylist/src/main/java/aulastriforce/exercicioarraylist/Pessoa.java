package aulastriforce.exercicioarraylist;

public class Pessoa {

    private String nome;

    private String sobrenome;

    private int idade;

    private Endereco endereco;

    private Contato telefone;

    private Carro carro;

    public String getNome() {
        return this.nome;
    }

    public String getSobrenome() {
        return this.sobrenome;
    }

    public int getIdade() {
        return this.idade;
    }

    public Endereco getEndereco() {
        return this.endereco;
    }

    public Contato getTelefone() {
        return this.telefone;
    }

    public Carro getCarro() {
        return this.carro;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public void setTelefone(Contato telefone) {
        this.telefone = telefone;
    }

    public void setCarro(Carro carro) {
        this.carro = carro;
    }
}