package aulastriforce.exercicioarraylist;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExercicioarraylistApplication {

	public static void main(String[] args) {

		Scanner scn = new Scanner(System.in);

		System.out.println("Quantidade de pessoas a serem cadastradas: ");
		int qttPessoas = scn.nextInt();
		List<Pessoa> pessoas = new ArrayList<>();

		for (int i = 0; i < qttPessoas; i++) {
			int contador = i + 1;
			Pessoa pessoa = new Pessoa();
			Endereco endereco = new Endereco();
			Contato contato = new Contato();
			Carro carro = new Carro();

			System.out.println(String.format("%s - Digite o nome: ", contador));
			pessoa.setNome(scn.next());
			System.out.println("Digite o sobrenome: ");
			pessoa.setSobrenome(scn.next());
			System.out.println("Digite a idade: ");
			pessoa.setIdade(scn.nextInt());

			System.out.println("Digite a rua: ");
			endereco.setRua(scn.next());
			System.out.println("Digite a numero: ");
			endereco.setNumero(scn.nextInt());
			System.out.println("Digite a bairro: ");
			endereco.setBairro(scn.next());
			System.out.println("Digite a cidade: ");
			endereco.setCidade(scn.next());

			System.out.println("Digite o DDD: ");
			contato.setDdd(scn.next());
			System.out.println("Digite o número: ");
			contato.setNumero(scn.nextInt());
			System.out.println("Digite o tipo de contato: ");
			contato.setTipo(scn.next());
			
			System.out.println("Digite o modelo: ");
			carro.setModelo(scn.next());			
			System.out.println("Digite o ano: ");
			carro.setAno(scn.nextInt());
			System.out.println("Digite a cor: ");
			carro.setCor(scn.next());
			System.out.println("Digite a placa: \n");
			carro.setPlaca(scn.next());			

			pessoa.setEndereco(endereco);
			pessoa.setTelefone(contato);
			pessoa.setCarro(carro);

			pessoas.add(pessoa);
		}

		for (int i = 0; i < qttPessoas; i++) {
			Pessoa pessoa = pessoas.get(i);
			Endereco endereco = pessoa.getEndereco();
			Contato contato = pessoa.getTelefone();
			Carro carro = pessoa.getCarro();

			System.out.println("Pessoa: \r");
			System.out.println(String.format("nome: %s \r", pessoa.getNome()));
			System.out.println(String.format("sobrenome: %s \r", pessoa.getSobrenome()));
			System.out.println(String.format("idade: %s \n", pessoa.getIdade()));

			System.out.println("Endereço: \r");
			System.out.println(String.format("rua: %s \r", endereco.getRua()));
			System.out.println(String.format("número: %s \r", endereco.getNumero()));
			System.out.println(String.format("bairro: %s \r", endereco.getBairro()));
			System.out.println(String.format("cidade: %s \n", endereco.getCidade()));

			System.out.println("Contato: \r");
			System.out.println(String.format("(%s), (%s), (%s)\n", contato.getDdd(), contato.getNumero(), contato.getTipo()));

			System.out.println("Carro: \r");
			System.out.println(String.format("modelo: %s \r", carro.getModelo()));
			System.out.println(String.format("ano: %s \r", carro.getAno()));
			System.out.println(String.format("cor: %s \r", carro.getCor()));
			System.out.println(String.format("placa: %s \n", carro.getPlaca()));	

		}

	}

}