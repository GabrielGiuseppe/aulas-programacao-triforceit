package com.aulastriforce.exercicioaula2704;

import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Exercicioaula2704Application {

	public static void main(String[] args) {
		SpringApplication.run(Exercicioaula2704Application.class, args);

		Scanner scn = new Scanner(System.in);

		System.out.println("Digite o primeiro número real:");
		double primeiroNumeroReal = scn.nextDouble();

		System.out.println("Digite o segundo número real:");
		double segundoNumeroReal = scn.nextDouble();

		System.out.println("Digite uma das opções de caractere-> (- + * /)");
		String caractere = scn.next();

		double resultado = 0;

		switch (caractere) {

		case "-":
			resultado = primeiroNumeroReal - segundoNumeroReal;
			break;
		case "+":
			resultado = primeiroNumeroReal + segundoNumeroReal;
			break;
		case "*":
			resultado = primeiroNumeroReal * segundoNumeroReal;
			break;
		case "/":
			resultado = primeiroNumeroReal / segundoNumeroReal;
			break;
		
		}

		System.out.println(
				String.format("resultado da operação =: %,.2f %s %,.2f = %,.2f", primeiroNumeroReal, caractere, segundoNumeroReal, resultado));

	}

}
