package com.aulastriforce.exercicioternarioaluno;

public class Aluno {

    private String nome;

    private int serie;

    private String matricula;

    private double notaPrimeiroSemestre;

    private double notaSegundoSemestre;

    public String getNome() {
        return this.nome;
    }

    public int getSerie() {
        return this.serie;
    }

    public String getMatricula() {
        return this.matricula;
    }

    public Double getNotaPrimeiroSemestre() {
        return this.notaPrimeiroSemestre;
    }

    public Double getNotaSegundoSemestre() {
        return this.notaSegundoSemestre;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setSerie(int serie) {
        this.serie = serie;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public void setNotaPrimeiroSemestre(Double notaPrimeiroSemestre) {
        this.notaPrimeiroSemestre = notaPrimeiroSemestre;
    }

    public void setNotaSegundoSemestre(Double notaSegundoSemestre) {
        this.notaSegundoSemestre = notaSegundoSemestre;
    }
}
