package com.aulastriforce.exercicioternarioaluno;

import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExercicioternarioalunoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExercicioternarioalunoApplication.class, args);

		Scanner scn = new Scanner(System.in);

		Aluno aluno = new Aluno();

		System.out.println("Digite o nome:");
		aluno.setNome(scn.next());

		System.out.println("Digite a série:");
		aluno.setSerie(scn.nextInt());

		System.out.println("Digite a matrícula:");
		aluno.setMatricula(scn.next());

		System.out.println("Digite a nota do Primeiro Semestre:");
		aluno.setNotaPrimeiroSemestre(scn.nextDouble());

		System.out.println("Digite a nota do Segundo Semestre:");
		aluno.setNotaSegundoSemestre(scn.nextDouble());

		Double mediaNota = (aluno.getNotaPrimeiroSemestre() + aluno.getNotaSegundoSemestre() / 2);

		System.out.println(String.format("nome: [%s] matricula: [%s] serie: [%s]", aluno.getNome(),
				aluno.getMatricula(), aluno.getSerie()));
		System.out.println("Notas:");
		System.out.println(String.format("Primeiro Semestre: [%s]", aluno.getNotaPrimeiroSemestre()));
		System.out.println(String.format("Segundo Semestre: [%s]", aluno.getNotaSegundoSemestre()));

		if (aluno.getSerie() <= 5 && mediaNota >= 5) {
			System.out.println(String.format("Declaro por este boletim que [%s] esta aprovado!", aluno.getNome()));
		}

		else if (aluno.getSerie() <= 5 && mediaNota < 5) {
			System.out.println(String.format("Declaro por este boletim que [%s] esta reprovado!", aluno.getNome()));
		}

		if (aluno.getSerie() >= 6 && aluno.getSerie() <= 8 && mediaNota >= 6) {
			System.out.println(String.format("Declaro por este boletim que [%s] esta aprovado!", aluno.getNome()));
		}

		else if (aluno.getSerie() >= 6 && aluno.getSerie() <= 8 && mediaNota < 6) {
			System.out.println(String.format("Declaro por este boletim que [%s] esta reprovado!", aluno.getNome()));
		}

	}

}
