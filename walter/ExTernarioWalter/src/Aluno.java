public class Aluno {

    String nome;
    int serie, matricula;
    double nota1semestre, nota2semestre;

    public Aluno(String nome, int serie, int matricula, double nota1semestre, double nota2semestre) {
        this.nome = nome;

        this.serie = serie;
        this.matricula = matricula;
        this.nota1semestre = nota1semestre;
        this.nota2semestre = nota2semestre;
    }

    public String getNome() {
        return nome;
    }

    public int getSerie() {
        return serie;
    }

    public double getNota1semestre() {
        return nota1semestre;
    }

    public double getNota2semestre() {
        return nota2semestre;
    }

    public int getMatricula() {
        return matricula;
    }

    public String calculofinal(double nota1semestre, double nota2semestre, int serie) {
        double notafinal = nota1semestre + nota2semestre;
        boolean boleano;
        boleano = (serie >= 5) ? true : false;

        if (boleano == true) {
            if (notafinal >= 5.0) {
                return "Aprovado";
            } else {
                return "Reprovado";
            }
        } else {
            if (notafinal >= 6.0) {
                return "Aprovado";
            } else {
                return "Reprovado";
            }
        }


    }

}