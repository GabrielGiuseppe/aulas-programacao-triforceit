   // Fazer um programa que imprima a média aritmética dos números 8,9 e 7. A média
   // dos números 4, 5 e 6. A soma das duas médias. A média das médias.


   import java.util.Scanner;

   public class ex2 {
       public static void main(String[] args) throws Exception {
   
        
   
           int n1 = 8;
           int n2 = 9;
           int n3 = 7;
   
           int calculaPrimeiraMedia = (n3 + n2 + n1) / 3;
   
           int n4 = 4;
           int n5 = 5;
           int n6 = 6;
   
           int calculaSegundaMedia = (n4 + n5 + n6) / 3;
   
           int SomaDaDuasMedias = calculaPrimeiraMedia + calculaSegundaMedia;
   
           int resultadoMediaDasMedias = (SomaDaDuasMedias) / 2;
   
           System.out.println("calculaPrimeiraMedia = " + calculaPrimeiraMedia + " calculaSegundaMedia = "
                   + calculaSegundaMedia + "  SomaDaDuasMedias = " + SomaDaDuasMedias + " resultadoMediaDasMedias = "
                   + resultadoMediaDasMedias);
       }
   }
   