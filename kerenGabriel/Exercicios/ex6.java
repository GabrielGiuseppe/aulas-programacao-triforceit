import java.util.Scanner;

public class ex6{

    //Desenvolva um algoritmo em Java que leia um número inteiro e imprima o seuantecessor e seu sucessor.//

      public static void main(String[] args) {
        
           Scanner scn = new Scanner(System.in);
    
            System.out.println("Digite um número inteiro:");
            int numeroInteiro = scn.nextInt();
    
            int numeroSucessor = (numeroInteiro + 1);
    
            int numeroAntecessor = (numeroInteiro - 1);
    
            System.out.println(String.format("O numero sucessor é: %s, O número antecessor é: %s", numeroSucessor, numeroAntecessor));
            
        }
    
    }