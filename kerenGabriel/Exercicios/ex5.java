//Crie um algoritmo que leia o valor do salário mínimo e o valor do salário de um usuário,calcule a quantidade de salários mínimos esse usuário ganha e imprima o resultado.(1SM=R$788,00)//

import java.util.Scanner;



public class ex5 {

	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);

		System.out.println("Digite salário mínimo:");
		double salarioMinimo = scn.nextDouble();

		System.out.println("Digite seu salário:");
		double salarioUsuario = scn.nextDouble();

	
		double totalSalariosMinimos = (salarioUsuario/salarioMinimo);
		
		System.out.println(String.format("Total salário mínimos: %s", totalSalariosMinimos));

	}

}
