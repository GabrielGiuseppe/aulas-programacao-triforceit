import java.util.Scanner;

public class ex3 {
    public static void main(String[] args) {
        double saldo;

        Scanner teclado = new Scanner(System.in);
        System.out.println("Informe o saldo  ");
        saldo = teclado.nextDouble();
        System.out.println("Foi informado saldo de " + saldo);
        teclado.close();

        System.out.println("O saldo reajustado é " + (saldo * 1.01));
    }
}