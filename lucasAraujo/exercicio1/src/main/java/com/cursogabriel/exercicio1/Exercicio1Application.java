package com.cursogabriel.exercicio1;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Exercicio1Application {

  public static void main(String[] args) {
    SpringApplication.run(Exercicio1Application.class, args);

    Scanner scn = new Scanner(System.in);
	LocalDate dataAtual = LocalDate.now();

    System.out.println("Digite Data Nascimento:");
    String dataNascimentoTexto = scn.next();

	LocalDate dataNascimento = LocalDate.parse(dataNascimentoTexto);

	Long operacao = dataNascimento.until(dataAtual, ChronoUnit.DAYS);

	System.out.println(String.format("Seus Dias De Vida é: %s", operacao));

  }
}
